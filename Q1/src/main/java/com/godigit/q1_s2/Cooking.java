//This program is an example of annotation based configuration
//"cooking" is the class which calls "biriyani" class which has a powder (object) of class "masala".
//"masala" has 3 different spices.
//"biriyani" is configured using 'context:annotation-config'and 'context:component-scan' tags in beans1.xml file.
//object "powder" is autowired and class "biriyani" is made component.


package com.godigit.q1_s2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Cooking {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans1.xml");
		Biriyani food =(Biriyani)context.getBean("biriyani");
		System.out.println("This is "+food.getRice()+" rice "+food.getMeat()+" biriyani \n ");
		System.out.println(food.getPowder());
		

	}

}
