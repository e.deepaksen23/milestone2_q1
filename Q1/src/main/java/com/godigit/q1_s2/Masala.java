package com.godigit.q1_s2;

public class Masala {
private String spice1;
private String spice2;
private String spice3;
public String getSpice1() {
	return spice1;
}
public void setSpice1(String spice1) {
	this.spice1 = spice1;
}
public String getSpice2() {
	return spice2;
}
public void setSpice2(String spice2) {
	this.spice2 = spice2;
}
public String getSpice3() {
	return spice3;
}
public void setSpice3(String spice3) {
	this.spice3 = spice3;
}
@Override
public String toString() {
	return "Masala [spice1=" + spice1 + ", spice2=" + spice2 + ", spice3=" + spice3 + "]";
}


}
