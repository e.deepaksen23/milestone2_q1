package com.godigit.q1_s2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component("biriyani")
public class Biriyani {
 private String rice="BASMATHI";
 private String meat="MUTTON";
 @Autowired
 private Masala powder;
public String getRice() {
	return rice;
}
public void setRice(String rice) {
	this.rice = rice;
}
public String getMeat() {
	return meat;
}
public void setMeat(String meat) {
	this.meat = meat;
}
public Masala getPowder() {
	return powder;
}
public void setPowder(Masala powder) {
	this.powder = powder;
}
 
 
}
