
//SPRINK IOC [APPLICATION CONTEXT] WITH XML BASED CONFIGURATION
// I CREATED AN INTERFACE CALLED "DRINKS", DEFINED IT'S METHOD "DRINKING()" USING BOOST AND HORLICKS.
// I CALLED "DRINKS" IN "BREAKTIME" USING XML FILE.
package com.godigit.Q1_S1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BreakTime {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		Drinks drinks=(Drinks)context.getBean("drinks");
        drinks.drinking();

	}

}
