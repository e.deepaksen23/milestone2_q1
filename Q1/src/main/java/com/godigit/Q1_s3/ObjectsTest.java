/*this is an example for java based configuration.
 * ShapesConfig file is the replacement for beans.xml file.
 * this class is annotated as 'configuration' and a bean is created which returns object of class "Shapes".
 *  this object is then used to access the members of the class "shapes". 
 * */
package com.godigit.Q1_s3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ObjectsTest {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(ShapesConfig.class);
		Shapes shape = context.getBean(Shapes.class);
		System.out.println(shape.circle());
		System.out.println(shape.square());
		
	}

}
